INSERT INTO USER VALUES (1, 2, 'koks@gmail.com' , true , '$2y$12$UZ6dslZlssg2Oc/tWIUwG.HYJSA8QEKdriZazOoQhFh3iC2oLKKQ2');
INSERT INTO USER VALUES (2, 2, 'koks2@gmail.com' , true , '$2y$12$UZ6dslZlssg2Oc/tWIUwG.HYJSA8QEKdriZazOoQhFh3iC2oLKKQ2');
--$2y$12$UZ6dslZlssg2Oc/tWIUwG.HYJSA8QEKdriZazOoQhFh3iC2oLKKQ2 -> haslo


INSERT INTO AUTHORITY VALUES (1,  'koks@gmail.com' , 'COMPANY');
INSERT INTO AUTHORITY VALUES (2,  'koks2@gmail.com' , 'EMPLOYEE');
