package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
public class WorkingTime {

    //TODO trzeba sprawdzić czy tak na być dla widoków??
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID uuid;

    private Long employeeId;

    private BigDecimal dSeconds;

    @Temporal(TemporalType.DATE)
    private Date entryDay;
}
