package com.example.demo.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Data
public class DaysOfWork {

    //TODO trzeba sprawdzić czy tak na być dla widoków??
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID uuid;

    private Long employeeId;
    private String firstName;
    private String surname;
    private Long companyId;
    private int workDays;
}
