package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
public class DailyWorkingTime {

    //TODO trzeba sprawdzić czy tak na być dla widoków??
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID uuid;

    private Long employeeId;
    private Long companyId;
    private String firstName;
    private String surname;
    private BigDecimal dSeconds;

    @Temporal(TemporalType.DATE)
    private Date entryDay;
}
