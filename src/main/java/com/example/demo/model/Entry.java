package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
public class Entry {

    //TODO trzeba sprawdzić czy tak na być dla widoków??
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID uuid;

    private Long entryId;

    @Temporal(TemporalType.DATE)
    private Date entryDate;

    private Long employeeId;

    private int isExit;
}
