package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController

public class LoginController {

    @RequestMapping(value = "/login", method= RequestMethod.GET)
    public ModelAndView login(){
        return new ModelAndView("index.html");
}

    @RequestMapping(value = "/admin", method= RequestMethod.GET)
    public ModelAndView adminView(){
        return new ModelAndView("admin-dashboard.html");
    }


    @RequestMapping(value = "/regular", method= RequestMethod.GET)
    public ModelAndView regularView(){
        return new ModelAndView("regular-dashboard.html");
    }



}
