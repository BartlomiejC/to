package com.example.demo.repository;

import com.example.demo.model.Company;
import com.example.demo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Company, Long > {

    @Query(value = "SELECT * from Employee " +
           // "profession p where e.profession_id=p.id and " +
           // "p.company_id=(SELECT c.id FROM company c WHERE c.user_id= " +
          //  "( SELECT u.id from user u where u.email= ?1)" +
            ")", nativeQuery = true)
    List<Employee> getEmployeesForCompany();
}
